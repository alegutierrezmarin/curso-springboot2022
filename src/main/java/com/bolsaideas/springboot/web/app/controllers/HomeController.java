package com.bolsaideas.springboot.web.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	@GetMapping("/")
	public String home() {
//		return "redirect:/app/index";
//		return "redirect://google.com";
		return "forward:/app/index"; // los datos no se pierden no se reincia se usa para las paginas de inicio
	}
}
