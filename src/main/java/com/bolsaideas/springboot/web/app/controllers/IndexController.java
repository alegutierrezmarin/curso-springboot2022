package com.bolsaideas.springboot.web.app.controllers;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bolsaideas.springboot.web.app.models.Usuario;

@Controller
@RequestMapping("/app")
public class IndexController {

	// inyeccion desde application.properties
	@Value("${texto.indexController.index.titulo}")
	private String textoIndex;
	@Value("${texto.indexController.index.perfil}")
	private String textoPerfil;
	@Value("${texto.indexController.index.listar}")
	private String textoListar;
	
	@GetMapping({ "/index", "/", "", "/home" })
	// public String index(Model model) {
	// public String index(ModelMap model) {
	// public String index(Map<String, Object> map) {
	/*
	 * public ModelAndView index(ModelAndView mv) { //model.addAttribute("titulo",
	 * "hola alejandro desde Spring"); //map.put("titulo",
	 * "hola alejandro desde Spring desde un map"); mv.addObject("titulo",
	 * "hola alejandro desde Spring desde un ModelAndView"); //return "index";
	 * mv.setViewName("index"); return mv; }
	 */
	// el mas usado es Model
	public String index(Model model) {
		//model.addAttribute("titulo", "Hola alejandro desde Spring usando Model");
		model.addAttribute("titulo", textoIndex);
		return "index";
	}

	@RequestMapping("/perfil")
	public String perfil(Model model) {
		Usuario usuario = new Usuario();
		usuario.setNombre("Alejandro Milton");
		usuario.setApellido("Gutierrez");
		usuario.setEmail("alegutierrez@gmail.com");
		model.addAttribute("usuario", usuario);
		model.addAttribute("titulo", textoPerfil.concat(usuario.getNombre()));
		return "perfil";
	}

	@RequestMapping("/listar")
	public String listar(Model model) {
		/*
		 * List<Usuario> usuarios = new ArrayList<>(); usuarios.add(new
		 * Usuario("usuario1","apellidou1","usuario1@gmail.com")); usuarios.add(new
		 * Usuario("usuario2","apellidou2","usuario2@gmail.com")); usuarios.add(new
		 * Usuario("usuario3","apellidou3","usuario3@gmail.com"));
		 */

//		List<Usuario> usuarios = Arrays.asList(
//				new Usuario("usuario1","apellidou1","usuario1@gmail.com"),
//				new Usuario("usuario2","apellidou2","usuario2@gmail.com"),
//				new Usuario("usuario3","apellidou3","usuario3@gmail.com"),
//				new Usuario("usuario4","apellidou4","usuario4@gmail.com"),
//				new Usuario("usuario5","apellidou5","usuario5@gmail.com")
//				);

//		model.addAttribute("titulo", "Listado de usuarios general");
		model.addAttribute("titulo", textoListar);
//		model.addAttribute("usuarios",usuarios);
		return "listar";
	}

	@ModelAttribute("usuarios")
	public List<Usuario> poblarUsuarios() {
		List<Usuario> usuarios = Arrays.asList(new Usuario("usuario1", "apellidou1", "usuario1@gmail.com"),
				new Usuario("usuario2", "apellidou2", "usuario2@gmail.com"),
				new Usuario("usuario3", "apellidou3", "usuario3@gmail.com"),
				new Usuario("usuario4", "apellidou4", "usuario4@gmail.com"),
				new Usuario("usuario5", "apellidou5", "usuario5@gmail.com"));
		return usuarios;
	}
}
